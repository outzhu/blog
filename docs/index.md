---
Date: 2022-05-27
Author: Ada Codina
---

![Rancher](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fboxboat.com%2F2019%2F10%2F22%2Fdeploy-kubernetes-apps-with-rancher%2Ffeatured.jpg&f=1&nofb=1)

# Provisioning Rancher via Terraform on Digital Ocean

A while back we were interested in automating the setup process for [Rancher](https://rancher.com/)’s [RKE](https://rancher.com/products/rke) on a [Digital Ocean](https://www.digitalocean.com/)’s Droplet. 

We chose HashiCorp’s [Terraform](https://www.terraform.io/) but ran into a few problems provisioning a RKE cluster on the Droplet via Terraform main.tf file as RKE is unable to run without a fixed ip-address in the main.tf which we won’t have until Droplet is created. 

Here’s our solution for an Ubuntu created Droplet.

## Prerequisites:
- Digital Ocean account/token
- Terraform
- private key for [provisioners](https://www.terraform.io/language/resources/provisioners/file)

## Main.tf

We followed [Yeahshecodes](https://www.yeahshecodes.com/devops/create-a-digital-ocean-droplet-with-terraform)’s guide on provisioning a Droplet via Terraform. 

We separated the variables, outputs and providers to their own tf files as per Terraform best practices.

## ssh_setup.sh

First create a ssh_setup.sh in your Terraform/files folder, we will use this later to fix Docker/RKE access issues.

```
#!/bin/bash

ssh-keygen -t rsa -b 4096 -N '' <<<$'\n'
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys

sed -i '/AllowTcpForwarding/d' /etc/ssh/sshd_config
echo "AllowTcpForwarding yes" >> /etc/ssh/sshd_config
grep AllowTcpForwarding /etc/ssh/sshd_config

```

## droplet_cloud_init.yaml

Next create a [cloud-init](https://cloudinit.readthedocs.io/en/latest/index.html#) yaml in the Terraform/files folder so we can install Docker.

```
#cloud-config

apt:
  sources:
    docker.list:
      source: deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable
      keyid: 9DC858229FC7DD38854AE2D88D81803C0EBFCD88

packages:
  - docker-ce
  - docker-ce-cli

```

We then add the [template](https://registry.terraform.io/providers/hashicorp/template/latest/docs) to the main.tf.

```
data "template_file" "droplet_cloud_init" {
  template = file("${path.module}/files/droplet_cloud_init.yaml")
}

resource "digitalocean_droplet" "rancher1" {
  image  = "ubuntu-20-04-x64"
  name   = "rancher1"
  region = ""
  size   = ""
  ssh_keys = ["${data.digitalocean_ssh_key.default.fingerprint}"]
  user_data = data.template_file.droplet_cloud_init.rendered
}
```
## Provisioners

Next add a [null_resource](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) so we can use [Provisioners](https://www.terraform.io/language/resources/provisioners/file).

```
resource "null_resource" "rke" {
  connection {
            host = "${digitalocean_droplet.rancher1.ipv4_address}"
            type = "ssh"
            user = "root"
            private_key = file("${path.module}/files/id_rsa")
            agent = false
        }

 provisioner "file" {
    source      = "${path.module}/files/ssh_setup.sh"
    destination = "/tmp/ssh_setup.sh"
  }

provisioner "remote-exec" {
    inline = [
    "chmod +x /tmp/ssh_setup.sh",
    "/tmp/ssh_setup.sh",
    "rm /tmp/ssh_setup.sh",
    "wget https://github.com/rancher/rke/releases/download/v1.3.2/rke_linux-amd64",
    "mv rke_linux-amd64 rke",
    "chmod +x rke",
    "printf '%s\n' 'nodes:' '  - address:' '    user: root' '    docker_socket: /var/run/docker.sock' '    role:' '      - controlplane' '      - etcd' '      - worker' '    ssh_key_path: ~/.ssh/id_rsa' >cluster.yml",
    "public_ip=$(curl ifconfig.co)",
    "sed -i \"s/address:/address: $public_ip/\" cluster.yml",
    "sudo snap install helm --classic",
    "snap install kubectl --classic",
    "mkdir ~/.kube",
    "helm repo add rancher-stable https://releases.rancher.com/server-charts/stable",
    "helm repo add jetstack https://charts.jetstack.io",
    "helm repo update",
    ]
  }
}

```

An error will concur if using ssh-setup commands in the provisioner "remote-exec" the workaround for this is the provisioner "file" and a .sh file.

The provisioner "remote-exec" will run the ssh-setup.sh to fix Docker/RKE, fetch the RKE version 1.3.2, make a cluster.yml with the Droplet ip, install Helm and Kubectl, create a Kubectl folder and add Helm Rancher repos.

We have automated the Rancher setup as much as possible if we tried to put ```./rke up``` in the main.tf file, Terraform will throw errors.

The rest of setup has to be done in the Droplet.

## RKE Setup

```
./rke up
mkdir -p ~/.kube
cp kube_config_cluster.yml ~/.kube/config
```

Since we have already added the Helm Rancher repos, you just need to follow the second part of the Rancher [installation guide](https://rancher.com/docs/rancher/v2.5/en/installation/install-rancher-on-k8s/#2-create-a-namespace-for-rancher)

